sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller,MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.DemoMenu", {

		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		},
		onSubmitPress: function(){
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("FORM", "FORM_SUBMIT");
		}
	});

});