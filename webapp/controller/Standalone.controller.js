sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, JSONModel, MessageToast, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.Standalone", {

		onInit: function() {
			var ctrl = this;
			ctrl.getView().setModel(new JSONModel("model/departments.json"), "departments");
			ctrl.getView().setModel(new JSONModel("model/members.json"), "members");
			ctrl.getView().setModel(new JSONModel("model/leavetype.json"), "leavetypes");
			ctrl.getView().setModel(new JSONModel("model/leave.json"), "leave");
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("FORM", "FORM_SUBMIT", this.onSumit, this);
		},
		onSumit: function() {
			MessageToast.show("clicked2");
			var ctrl = this;
			var sDeptID = ctrl.getView().byId("deptID").getSelectedKey();
			var sApplcantID = ctrl.getView().byId("applicantID").getSelectedKey();
			var sFillerID = ctrl.getView().byId("fillerID").getSelectedKey();
			var sAgentID = ctrl.getView().byId("agentID").getSelectedKey();
			var sLeaveType = ctrl.getView().byId("leaveType").getSelectedKey();
			var sFlexibleWork = ctrl.getView().byId("flexibleWork").getSelected();
			var oLeaveModel=ctrl.getView().getModel("leave");

			oLeaveModel.setProperty("/deptID",sDeptID);
			oLeaveModel.setProperty("/applicantID",sApplcantID);
			oLeaveModel.setProperty("/fillerID",sFillerID);
			oLeaveModel.setProperty("/agentID",sAgentID);
			oLeaveModel.setProperty("/leaveType",sLeaveType);
			oLeaveModel.setProperty("/flexibleWork",sFlexibleWork);
			MessageToast.show(JSON.stringify(oLeaveModel.oData));
		},
		changeDepartment: function(oEvent) {
			var ctrl = this;
			var sDepartmentID = oEvent.getParameters().selectedItem.getKey();
			var aFilter = [];

			// build filter array
			aFilter.push(new Filter("departmentId", FilterOperator.Contains, sDepartmentID));

			// filter applicantID, agentID binding
			ctrl.getView().byId("applicantID").getBinding("items").filter(aFilter);
			ctrl.getView().byId("agentID").getBinding("items").filter(aFilter);
			
		}
		
	});

});