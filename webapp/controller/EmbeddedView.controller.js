sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.EmbeddedView", {

		onInit: function(){
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("FORM", "FORM_SUBMIT", this.onSumit, this);
		},
		onButtonPress: function(){
			sap.m.MessageToast.show("clicked");
		}

	});

});